@extends('layouts.layout')


@section('title')
  Create new product
@stop

@section('content')
<br><br>
<h1>Create Product</h1>
<div>
  <form action="{{route('product.store')}}" method="POST" >
    <div class="form-group">
      <input type="text" name="name">
      <input type="number" name="price">
      <button type="submit">Submit</button>
    </div>
  </form>
</div>

<br><br>
@stop
