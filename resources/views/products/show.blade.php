@extends('layouts.layout')


@section('title')
  Show product
@stop

@section('content')
<br><br>
<form action="{{route('product.destroy',$product->id)}}" method="POST" >


  <h1>{{$product->name}}</h1>
  <h3>{{$product->price}}</h3>

  <a href="{{route('product.edit', $product->id)}}">Edit</a>
  <input type="submit" value="delete">

  {{method_field('DELETE')}}

</form>
@stop
