@extends('layouts.layout')


@section('title')
  Edit product
@stop

@section('content')
<br><br>
<form action="{{route('product.update',$product->id)}}" method="POST" >
  <div class="form-group">
    <input type="text" name="name" value="{{$product->name}}">
    <input type="number" name="price" value="{{$product->price}}">
    <button type="submit">Edit</button>
  </div>

  {{method_field('PATCH')}}

</form>
@stop
