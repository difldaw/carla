@extends("layouts.layout")
@section("title")
  All products
@stop
@section("content")
  @foreach ($products as $product)
    <h1>{{$product->name}}</h1>
    <h3>{{$product->price}}</h3>
    <br/>
  @endforeach
  <a href="{{url('basicrouting')}}">Basic routing</a>
  <a href="{{route('product.create')}}">Create</a>

@stop
