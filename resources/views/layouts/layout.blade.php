<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>@yield('title')</title>
    <!-- CSS  -->
    <link href="{{ asset('/css/semantic.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/container.min.css') }}" rel="stylesheet">
    <script src="{{ asset('/js/semantic.min.js') }}"></script>
    <!-- Compiled and minified CSS -->

  </head>
  <body>

    @include('layouts.menu')
      <div class="">
        @yield('content')

      </div>


  </body>
</html>
