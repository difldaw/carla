
@extends('layouts.layout')


@section('title')
  About
@stop

@section('content')
<button class="ui primary button">
  Button
</button>
<div class="ui black button" tabindex="0">
  Focusable
</div>

<div class="ui four column doubling stackable grid container">
  <div class="column">
    <p>h</p>
    <p></p>
  </div>
  <div class="column">
    <p>g</p>
    <p></p>
  </div>
  <div class="column">
    <p>f</p>
    <p></p>
  </div>
  <div class="column">
    <p>l</p>
    <p></p>
  </div>
</div>
@stop
