<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/prueba', function () {
    return view('pages.prueba');
});

Route::get('about', [
    'as' => 'about',
    'uses' => 'PagesController@getAbout'
]);


Route::get('contact', 'PagesController@getContact')->name('contact');
/*
Route::get('contact', ['as' => 'contact', function () {
    return view('pages.contact');
}]);
*/
Route::get('basicrouting',function(){
  return 'THIS IS BASIC ROUTING';
  //return view('welcome');
});
Route::get('user/{id}',function($id){
  //$user = User::find($id);
  return $id;
  //return view('welcome');
});
Route::get('user/{id}/album/{aid}',function($id,$aid){
  //$user = User::find($id);
  return $id.' '.$aid;
  //return view('welcome');
});
Route::get('album/{aid?}',function($aid=null){
  //$user = User::find($id);
  return $aid;
  //return view('welcome');
});
Route::post('someurl',function(){
  //return 'THIS IS BASIC ROUTING';
  //return view('welcome');
  //no logic here
});
//http verbs
Route::match(['get','post','delete'],'matchrouting',function(){
  return 'matchrouting';
  //return view('welcome');
});
Route::any('anyroute',function(){
  return 'anyroute';
});

Route::resource('product', 'ProductController');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
