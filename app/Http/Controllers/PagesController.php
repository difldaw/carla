<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;

class PagesController extends Controller
{
  public function getAbout(){
    $companyName = "LDAW";
    $users = array("hugo","paco","luis");
    return view('pages.about')
    ->with("name", $companyName)
    ->with("users", $users);
  }
  public function getContact(){
    return view('pages.contact');
  }
  public function register(){
    echo "User";
  }

}
